package com.example.gm.databindingtask

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

class CricketAdapter (val items : ArrayList<Cricket>) : RecyclerView.Adapter<CricketAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): CricketAdapter.ViewHolder {
        val v = LayoutInflater.from(p0.context).inflate(R.layout.list_items, p0, false)
        return ViewHolder(v)

    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: CricketAdapter.ViewHolder, p1: Int) {
        p0.bindItems(items[p1])
    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(cricket: Cricket) {
            val icon = itemView.findViewById(R.id.info) as ImageView
            val txt = itemView.findViewById(R.id.truck_text) as TextView
            val truck = itemView.findViewById(R.id.truck) as ImageView


            icon.setImageResource(R.drawable.ic_info_outline_black_24dp)
            txt.text=cricket.text
            truck.setImageResource(R.drawable.truck)


        }
    }
}
