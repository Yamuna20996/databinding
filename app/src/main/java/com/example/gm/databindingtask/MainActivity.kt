package com.example.gm.databindingtask

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.widget.LinearLayout
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val list=ArrayList<Cricket>()

        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))
        list.add(Cricket(R.id.truck,"Truck",R.id.icon))


        recyclerView.setHasFixedSize(true)
        recyclerView.layoutManager= LinearLayoutManager(this, LinearLayout.VERTICAL,false)
        recyclerView.addItemDecoration(
            DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        );

        val cricket=CricketAdapter(list)
        recyclerView.adapter=cricket

    }
}
